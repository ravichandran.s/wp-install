#!/bin/bash 
clear
echo "********************************************"
echo "          WordPress Install Script          "
echo "********************************************"
echo
echo "*************Database details*************"
echo -n "Database Host (e.g localhost) : "
read -e dbhost
echo -n "Database Name : "
read -e dbname
echo -n "Database User : "
read -e dbuser
echo -n "Database Password : "
read -s pwd
echo -n "Re-enter Database Password : "
read -s repwd
if [ "$pwd" != "$repwd" ] ; then
    echo "Password mismatch and re enter password and confirm password"
    exit 1;
else
    dbpass=$repwd
fi

echo
echo "*************Admin details*************"
echo -n "Site url (e.g http://www.YOURSITEURL.COM/) : "
read -e siteurl
echo -n "Site Name (e.g YOUR WEBSITE NAME) : "
read -e sitename
echo -n "Email Address (e.g webmaster@YOURSITEURL.com) : "
read -e wpemail
echo -n "Admin User Name : "
read -e wpuser
echo -n "Admin User Password : "
read -s wppass
echo -n "run install? (y/n) : "
read run
if [ "$run" == n ] ; then
    exit
else
    echo
    echo "*********************************************"
    echo "A robot is now installing WordPress for you."
    echo "*********************************************"
    echo "Downloading wordpress latest version from https://wordpress.org/latest.tar.gz"
    curl -O https://wordpress.org/latest.tar.gz
    echo "Extracting tarball wordpress..."
    tar -zxvf latest.tar.gz
    #copy file to parent dir
    cp -rf wordpress/* .
    #remove files from wordpress folder
    rm -R wordpress
    #create wp config
    echo
    echo "Creating database configuration file..."
    cp wp-config-sample.php wp-config.php
    #set database details with perl find and replace
    perl -pi -e "s/localhost/$dbhost/g" wp-config.php
    perl -pi -e "s/database_name_here/$dbname/g" wp-config.php
    perl -pi -e "s/username_here/$dbuser/g" wp-config.php
    perl -pi -e "s/password_here/$dbpass/g" wp-config.php
    #create uploads folder and set permissions
    mkdir wp-content/uploads
    chmod 777 wp-content/uploads
    echo
    echo "Installing wordpress..."
    wp core install --url="$siteurl" --title="$sitename" --admin_user="$wpuser" --admin_password="$wppass" --admin_email="$wpemail"
    #remove zip file
    rm latest.tar.gz
    #remove bash script
    # rm wp-install.sh
    echo "========================="
    echo "Installation is complete."
    echo "========================="
    echo
    echo "Thankyou...linuxtweaks.in"
fi